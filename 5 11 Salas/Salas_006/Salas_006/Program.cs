﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salas_006
{
    class Program
    {
        static void Main(string[] args)
        {
            libreriafactura.factura objfactura = new libreriafactura.factura();
            objfactura.Numero = 1500;
            objfactura.Nombre = "Jorge";
            objfactura.Subtotal = 2500;
            objfactura. Total = (objfactura.Subtotal) * 0.2f;
            Console.WriteLine(objfactura.MostrarDatos());
            Console.WriteLine("");

            libreriafactura.factura objfactura2 = new libreriafactura.factura();
            Console.WriteLine(objfactura2.MostrarDatos());
            Console.WriteLine("");

            Console.WriteLine("El nuevo objeto con valores es: ");
            Console.WriteLine("");
            objfactura2.Numero = 1501;
            objfactura2.Nombre = "Pepe";
            objfactura2.Subtotal = 3000;
            objfactura2.Total =  (objfactura2.Subtotal) * 0.2f ;
            Console.WriteLine(objfactura2.MostrarDatos("Ingeniero"));

            Console.ReadLine();
        }
    }
}
