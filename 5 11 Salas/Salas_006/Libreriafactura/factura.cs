﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriafactura
{
    public class factura
    {
        public factura()
        {
            Numero = 1500;
        }
        //Atributos
        private int numero;
        private string nombre;
        private float subtotal;
        private float total;

        //Propiedades
        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public float Subtotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }

        public float Total
        {
            get {return total; }
            set {total = value; }
        }


        //Metodos

        public string MostrarDatos()
        {
            string strDatos;
            strDatos = Numero + " " + Nombre + " " + Subtotal + " " + Total;
            return strDatos;
        }

        public string MostrarDatos(string pTitulo)
        {
            string strDatos;
            strDatos = Numero + " " + Nombre + " " + Subtotal + "" + Total + "" + pTitulo;
            return strDatos;
        }

    }
}
