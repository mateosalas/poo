﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medidas
{
    public class Medida
    {
        public Medida()
        {
            Nombre = "Rectangulo 1"; //Este es el construct
        }

        //Atributos
        private string nombre;
        private int hipotenusa;
        private int ladob;
        private int ladoc;
        private int cuenta;

        //Propiedades 
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public int Hipotenusa
        {
            get { return hipotenusa; }
            set { hipotenusa = value; }
        }

        public int Ladob
        {
            get { return ladob; }
            set { ladob = value; }
        }

        public int Ladoc
        {
            get { return ladoc; }
            set { ladoc = value; }
        }

        //Metodos

        public string medidaRectangulo()
        {

            if ((Math.Pow(Hipotenusa, 2)) == (Math.Pow(Ladob, 2) + (Math.Pow(Ladoc, 2))))
            {
                return "Es rectangulo";
            }
            else {
                return "No es rectangulo";
            }
        }       

    }
}
