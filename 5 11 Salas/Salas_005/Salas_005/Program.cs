﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matriz
{
    class Matriz
    {
        private int[,] mat;

        public void Ingresar()
        {
            mat = new int[3, 4];
            for (int f = 0; f < 3; f++)
            {
                for (int c = 0; c < 4; c++)
                {
                    Console.Write("Ingrese Materia y notas [" + (f + 1) + "," + (c + 1) + "]: ");
                    string linea;
                    linea = Console.ReadLine();
                    if (linea = int) {
                        mat[f, c] = int.Parse(linea);
                    }
                    else {
                        mat[f, c] = linea;
                    }
                    
                }
            }
        }

        public void Imprimir()
        {
            for (int f = 0; f < 3; f++)
            {
                for (int c = 0; c < 4; c++)
                {
                    Console.Write(mat[f, c] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Matriz ma = new Matriz();
            ma.Ingresar();
            ma.Imprimir();
        }
    }
}