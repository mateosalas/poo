﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salas_002
{
    class Program
    {
        static void Main(string[] args)
        {
            //TP02: Incrementar un número entre 1 y 10 y no mostrarlo cuando sea 2,5 y 9. Acumar todos los valores en otra variable.

            int y = 0;

            for (int x = 1; x <= 10; x++)
            {
                y += x;
                if (x != 2 && x != 5 && x != 9)
                {
                   Console.WriteLine("Los numerosp: " + x);
                }
            }
            Console.WriteLine("El acumulado de lo numeros es: " + y);
            Console.WriteLine("Presione cualquier tecla para continuar...");
            Console.ReadKey(true);
        }
    }
}
