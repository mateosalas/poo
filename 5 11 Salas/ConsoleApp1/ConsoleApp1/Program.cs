﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            string[,] persona = new string[3,2];
            int[] edad = new int[3];

            persona[0,0] = "Pepe";
            persona[1,0] = "Jorge";
            persona[2,0] = "Pedro";

            persona[0, 1] = "Nieves";
            persona[1, 1] = "MOSTRI";
            persona[2, 1] = "picapiedra";

            edad[0] = 18;
            edad[1] = 25;
            edad[2] = 54;

            Console.WriteLine("Se mostraran los datos");

            for (int a = 0; a < 3; a++)
            {

                Console.WriteLine("{0} {1} de {2} años", persona[a,0], persona[a,1], edad[a]);

            }
            Console.ReadLine();
            /*
            Console.WriteLine(persona[0]);
            Console.WriteLine(persona[1]);
            Console.WriteLine(persona[2]);
            */
        }
    }
}
