﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace integrantes
{
    public class alumno
    {
        public alumno()
        {
        }
        //Atributos
        private string apellido;
        private string nombre;
        private int edad;
        private int dni;

        public string Apellido{
            get { return apellido; }
            set { apellido = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        public int DNI
        {
            get { return dni; }
            set { dni = value; }
        }

        //Metodos
        public string MostarDatos()
        {
            string strDatos;
            strDatos = Apellido + "" + Nombre + "" + Edad + "" + DNI;
            return strDatos;
        }
        



    }
}
