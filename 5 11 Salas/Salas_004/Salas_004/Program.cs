﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salas_004
{
    class Program
    {
        static void Main(string[] args)
        {
           //TP 04:Imprimir la suma de los números impares del 1 al 10.
           int y = 0;

           for (int x = 1; x <= 10; x = x + 2)
           {
               //Console.WriteLine("Numero impar:" + x);
               y = y + x;

           }
           Console.WriteLine("Suma de los numeros impares son:" + y);
           Console.ReadLine();
        }
    }
}
