﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console01{
    class Program{
        static void Main(string[] args){
            Persona.Class01[] objArreglo = new Persona.Class01[3];
            objArreglo[0] = new Persona.Class01();
            objArreglo[1] = new Persona.Class01();
            objArreglo[2] = new Persona.Class01();

            objArreglo[0].Nombre = "Candela";
            objArreglo[0].Apellido = "La moto";
            objArreglo[0].Edad = 10;

            objArreglo[1].Nombre = "Adolf";
            objArreglo[1].Apellido = "El nazi";
            objArreglo[1].Edad = 250349;

            objArreglo[2].Nombre = "Franquito";
            objArreglo[2].Apellido = "Dobla2";
            objArreglo[2].Edad = 50;
            //Console.WriteLine(objArreglo[0].Nombre +"\t"+ objArreglo[0].Apellido +"\t Tiene: " + objArreglo[0].Edad);
            Console.WriteLine(objArreglo[0].MostrarDatos());
            Console.ReadLine();

        }
    }
}
