﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persona{
    public class Class01{
        //Constructor
        public Class01(){
        }
        //Atributos
        private string apellido;
        private string nombre;
        private int edad;

        //Propiedades
        public string Apellido{
            get { return apellido; }
            set { apellido = value; }
        }

        public string Nombre{
            get { return nombre; }
            set { nombre = value; }
        }

        public int Edad{
            get { return edad; }
            set { edad = value; }
        }
        
        //Método
        public string MostrarDatos(){
            string str;
            str = Apellido + " \t" + Nombre + " \t" + "Tiene "+ Edad + " años"; 
            return str;
        }
    }
}
