﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Almacenar
{
    public class Datos
    {
        public Datos()
        {
            Apellido = "Invitado";
        }

        //Atributos
        private string apellido;
        private string nombre;
        private int edad;
        private int mesesejer;

        //Propiedades
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }
        public int Mesesejer
        {
            get { return mesesejer; }
            set { mesesejer = value; }
        }
        //Metodos
        public string MostrarDatos()
        {
            string strDatos;
            strDatos =  "Ejercicio B. Su apellido es " + Apellido + " , su nombre es " + Nombre + ", su edad es " + edad + " y trabaja hace " + mesesejer + " meses";
            return strDatos;
        }

        public string MostarDatos(string pTitulo)
        {
            string strDatos;
            strDatos = pTitulo + " " + Apellido + " " + Nombre + " " + Edad + " " + Mesesejer;
            return strDatos;
        }
    }
}
