﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consola01
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("holaaaaaaaaaaaaaaaaaaaaa");
            Integrantes.Alumno objAlumno = new Integrantes.Alumno();
            objAlumno.Apellido = "Perez Garcia";
            objAlumno.Nombre = "Yamila";
            objAlumno.Edad = 25;
            objAlumno.Dni = 30650124;
            Console.WriteLine( objAlumno.MostrarDatos() );
            Console.WriteLine(" ");

            Integrantes.Alumno objAlumno2 = new Integrantes.Alumno();            
            Console.WriteLine(objAlumno2.MostrarDatos());
            Console.WriteLine(" ");

            Console.WriteLine("el nuevo objeto con valores es: ");
            Console.WriteLine(" ");
            objAlumno2.Apellido = "Rodriguez";
            objAlumno2.Nombre = "Leandro";
            objAlumno2.Edad = 30;
            objAlumno2.Dni = 37680164;
            Console.WriteLine(objAlumno2.MostrarDatos("Ingeniero"));
            Console.ReadLine();


        }
    }
}
