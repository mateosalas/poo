﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integrantes
{
   public class Alumno
    {
        public Alumno()
        {
            Apellido = "Invitado"; //Este es el construct
        }

        //atributos
        private string apellido;
        private string nombre;
        private int edad;
        private int dni;

        // propiedades
        public string Apellido
        {
            get { return apellido;}
            set { apellido = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public int Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }

        // metodos

        public string MostrarDatos()
        {
            string strDatos;
            strDatos = Apellido + " " + Nombre + " " + Edad + " " + Dni;
            return strDatos;
        }

        public string MostrarDatos(string pTitulo)
        {
            string strDatos;
            strDatos = pTitulo +" "+Apellido + " " + Nombre + " " + Edad + " " + Dni;
            return strDatos;
        }


    }
}
